# Flask-FontAwesome

Ready-to-use FontAwesome for use in Flask. Heavily inspired by [flask-bootstrap](https://github.com/mbr/flask-bootstrap).


## Usage

### Install flask-fontawesome from a git repo

Filename: requirements.txt
    
    flask

    # Example: load from git repo on local filesystem
    -e git+file:///path/to/flask-fontawesome#egg=flask_fontawesome

    # Example: load branch/gittish from git repo on local filesystem
    -e git+file:///path/to/flask-fontawesome@GITTISH#egg=flask_fontawesome

    # Example: Load from git repo hosted on remote
    -e git+git@gitlab.com:marianopeterson/flask-fontawesome.git#egg=flask_fontawesome

Use virtualenv and pip to install the flask-fontawesome package

    virtualenv env
    source env/bin/activate
    pip install -r requirements.txt


### Use the flask-fontawesome in a Flask app

Filename: app.py

    from flask import Flask
    from flask_fontawesome import FontAwesome, url_for_fontawesome

    app = Flask(__name__)
    FontAwesome(app)
    app.config.update(
            'FONTAWESOME_SERVE_LOCAL': True,
            'FONTAWESOME_USE_MINIFIED': True)

    @app.route('/')
    def index():
        return url_for_fontawesome('css/font-awesome.css')

    @app.route('/third_party_cdn')
    def third_party():
        app.config['FONTAWESOME_SERVE_LOCAL'] = False
        return url_for_fontawesome('css/font-awesome.css')

    @app.route('/url_for_example')
    def url_for_example():
        return url_for('fontawesome.static', filename='css/font-awesome.css')

Run flask:

    source env/bin/activate
    pip install -r requirements.txt
    FLASK_APP=app.py FLASK_DEBUG=1 flask run --port 9000

    curl "http://127.0.0.1:9000/"
    curl "http://127.0.0.1:9000/static/fontawesome/css/font-awesome.css"

