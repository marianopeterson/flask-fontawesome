import re

from flask import Blueprint, current_app, url_for


__version__ = '4.7.0.dev1'
FONTAWESOME_VERSION = re.sub(r'^(\d+\.\d+\.\d+).*', r'\1', __version__)


class CDN(object):
    """Base class for CDN objects."""

    def get_resource_url(self, filename):
        """Return resource url for filename."""
        raise NotImplementedError


class StaticCDN(object):
    """A CDN that serves content from the local application."

    :param static_endpoint: Endpoint to use
    :param rev: If `True`, honor ``FONTAWESOME_QUERYSTRING_REVVING``.
    """

    def __init__(self, static_endpoint='static', rev=False):
        self.static_endpoint = static_endpoint
        self.rev = rev

    def get_resource_url(self, filename):
        extra_args = {}

        if self.rev and current_app.config['FONTAWESOME_QUERYSTRING_REVVING']:
            extra_args['fontawesome'] = __version__

        return url_for(self.static_endpoint, filename=filename, **extra_args)


class WebCDN(object):
    """Serves files from the web.

    :param baseurl: The baseurl. Filenames are simply appended to this URL.
    """

    def __init__(self, baseurl):
        self.baseurl = baseurl

    def get_resource_url(self, filename):
        return self.baseurl + filename


class ConditionalCDN(object):
    """Serves files from one CDN or another, depending on whether a
    configuration value is set.

    :param confvar: Configuration variable to use
    :param primary: CDN to use if the Configuration variable is ``True``.
    :param fallback: CDN to use otherwise.
    """

    def __init__(self, confvar, primary, fallback):
        self.confvar = confvar
        self.primary = primary
        self.fallback = fallback

    def get_resource_url(self, filename):
        if current_app.config[self.confvar]:
            return self.primary.get_resource_url(filename)
        return self.fallback.get_resource_url(filename)


def url_for_fontawesome(filename, cdn, use_minified=None, local=True):
    """Resource finding function, also available in templates.

    Tries to find a resource, will force SSL depending on
    ``FONTAWESOME_CDN_FORCE_SSL`` settings.

    :param filename: File to find a URL for.
    :param cdn: Name of the CDN to use.
    :param use_minified: If set to ``True``/``False``, use/don't use
                         minified. If ``None``, honors
                         ``FONTAWESOME_USE_MINIFIED``.
    : param local: If ``True``, uses the ``local`` CDN when
                   ``FONTAWESOME_SERVE_LOCAL`` is enabled. If ``False``, uses
                   the ``static`` CDN instead.
    :return: A URL.
    """
    config = current_app.config

    if None == use_minified:
        use_minified = config['FONTAWESOME_USE_MINIFIED']

    if use_minified:
        filename = '%s.min.%s' % tuple(filename.rsplit('.', 1))

    cdns = current_app.extensions['fontawesome']['cdns']
    resource_url = cdns[cdn].get_resource_url(filename)

    if resource_url.startswith('//') and config['FONTAWESOME_CDN_FORCE_SSL']:
        resource_url = 'https:%s' % resource_url

    return resource_url


class FontAwesome(object):

    def __init__(self, app=None):
        if app is not None:
            self.init_app(app)

    def init_app(self,app):
        app.config.setdefault('FONTAWESOME_USE_MINIFIED', True)
        app.config.setdefault('FONTAWESOME_CDN_FORCE_SSL', False)
        app.config.setdefault('FONTAWESOME_QUERYSTRING_REVVING', True)
        app.config.setdefault('FONTAWESOME_SERVE_LOCAL', False)
        app.config.setdefault('FONTAWESOME_LOCAL_SUBDOMAIN', None)

        blueprint = Blueprint(
            'fontawesome',
            __name__,
            static_folder='static',
            static_url_path=app.static_url_path + '/fontawesome',
            subdomain=app.config['FONTAWESOME_LOCAL_SUBDOMAIN'])

        app.register_blueprint(blueprint)

        if not hasattr(app, 'extensions'):
            app.extensions = {}

        local = StaticCDN('fontawesome.static', rev=True)
        static = StaticCDN()

        def lwrap(cdn, primary=static):
            return ConditionalCDN('FONTAWESOME_SERVE_LOCAL', primary, cdn)

        third_party = lwrap(
            WebCDN('//cdnjs.cloudflare.com/ajax/libs/font-awesome/%s/' %
                FONTAWESOME_VERSION), local)

        app.extensions['fontawesome'] = {
            'cdns': {
                'local': local,
                'static': static,
                'third_party': third_party
            },
        }

