from setuptools import setup


DESCRIPTION = 'A Flask extension that includes FontAwesome in your project.'


setup(name='Flask-FontAwesome',
      version='1.0.0',
      description=DESCRIPTION,
      url='',
      packages=['flask_fontawesome'],
      zip_safe=False,
      include_package_data=True,
      install_requires=['Flask>=0.8']
)
